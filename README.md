# M2 TIW : Gestion de Grandes Masses de Données (GGMD)


## Ressources
 1. [Introduction et Séance de remise à niveau](./seance1/) 
 2. [Masses de données et SGBD](./seance2/)
 3. []()
 4. []()
 5. []()
 6. []()
 7. [Flux de données](./seance7/)



## Calendrier
 - [Lien ADE](https://adelb.univ-lyon1.fr/portal/?originPage=calendar)


## Modalités de Contrôle Continu (MCC)
 - Contrôle Terminal (ECA) : 60% de la note finale
 - 3 QCMs : 3 x 5% 
 - TP noté : 25%

## Annales
 - [CT 2022](./Annales/CT2022.pdf)
 - [CT 2023](./Annales/CT2023.pdf)