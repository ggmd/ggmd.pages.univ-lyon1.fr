# GGMD : Gestion de Grandes Masses de Données
## TP1 : Performance, Tuning & Sharding.

L'objectif de ce TP est de vous confronter à l'exécution de requêtes coûteuses et de voir comment améliorer ou dans certains cas, permettre, l'exécution d'une requête couteuse intérrogeant de données réparties.


On considère les données brutes de l'INSEE parties comme suit:
- Sur GGMD1 : mairies_2020
- Sur GGMD2 : communes_2024, departement_2024, region_2024
- Sur GGMD3 : personne_insee


On s'intéresse pour la requête Q1 suivante :

Lister les personnes nommées MARTIN, décédées durant l'année 2000 et qui sont nées en région 'Auvergne-Rhône-Alpes' et décédée en région 'Centre-Vale de Loire'. Le résultat s'affichera selon le schéma (nomprenom, sexe, datenaiss, com_nais, adr_mair_nais, dep_nais, reg_nais, com_deces, adr_mair_deces, dep_deceq, reg_deces)

où : 	
- *com_nais* correspond au libellé de la commune de naissance,  
- *adr_mair_nais* correspond à l'adresse de la commune de naissance,
- *dep_nais* correspond au libellé du département de naissance,
- *reg_nais* correspond au libellé de la région de naissance,
- *com_deces* correspond au libellé de la commune de décès,  
- *adr_mair_deces* correspond à l'adresse de la commune de décès,
- *dep_deces* correspond au libellé du département de décès,
- *reg_deces* correspond au libellé de la région de décès,



> TODO 01 : écrire la requête Q1 en SQL en prévision de l'exécuter sur GGMD3.


> TODO 02 : testez votre requête. Combien de n-uplets obtenez-vous?


> TODO 03 : Donnez le plan d'exécution associée à l'excution précédente (version output dans terminal, version graphique DALIBO).


> TODO 04 : Répondre aux questions suivantes :
> 1. Quelle est l'opération la plus couteuse du plan d'exécution ?
> 2. Quel est l'ordre de grandeur des coûts de traitements des actions effectuées en local ?
> 3. Quel est l'ordre de grandeur des coûts d'inerrogation des *foreign tables* ?
> 4. Quelles sont les requêtes réellement effectuées sur GGMD1 et GGMD2 ?


> TODO 05 : définir les FOREIGN TABLE nécessaires à l'exécution de Q1 sur GGMD1


> TODO 06 : Réécrire la requête Q1 de manière à pouvoir l'exécuter depuis GGMD1.


### Partie 2 : Focus sur l'indexation
L'objectif de cette partie est de voir les conséquences de la création d'index sur les données. L'espace disque nécessaire à la création d'un index peut devenir problématique.

> TODO 07 :  Proposer un index pertinent pour améliorer la requête Q1.

> TODO 08 : Créer l'index et regardez l'impact sur le plan d'exécution.


### Partie 3 : Focus sur le paramétrage du SGBD
L'objectif de cette partie est de voir en quoi le paramètrage du SGBD impacte les performances

> TODO 09 : À partir des notioins vues en cours, testez la requête Q1 en modifiant certains paramètres du fichier `/etc/postgresql/16/main/postgresql.conf`. Le SGBD vous semble-t-il correctement configuré?


### Partie 4 : Focus sur le sharding
L'objectif de cette partie est de voir en quoi l'utilisation du sharding permet d'améliorer les performances d'exécution.

Afin de traiter les questions suivantes, vous êtes invités à parcourir la documentation [ici](https://docs.citusdata.com/en/v12.1/)


> TODO 10 : Après avoir activé l'extension citus `CREATE EXTENSION citus;` sur les 3 instances de posgres, ajoutez GGMD1 et GGMD2 comme noeud de traitement sur GGMD3 qui sera le noeud master(`master_add_node()`).


 Nous alons créer une table `pers2000` ayant le même schéma que `personne_insee` qui ne contiendra que les personnes décédées durant l'année 2000.

> TODO 11 : Créer la table `pers2000`.

> TODO 12 : Déclarer la table `pers2000` comme come une table distribuée (`create_distributed_table`), puis observait les *shards* créés(`pg_dist_shard`) et leur placement(`pg_dist_shard_placement`). Combien de *shards* ont été créés par défaut? Combien sont sur GGMD1? Combien sont sur GGMD2?


> TODO 13 : Insérer les 55 0135 n-uplets pertinents issus de `personne_insee` dans `pers2000`.


> TODO 14 : Réécrire la requête Q1 depuis GGMD3 en prenant en compte de pers2000. Analysez le plan d'exécution.


> BONUS 15 : créer la table distribuée `personne_insee_dist` qui cette fois contiendra tous les n-upets de `personne_insee`. Réévaluez les plans d'exécution précédents pour comparer.






